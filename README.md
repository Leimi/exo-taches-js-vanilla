# Exercice : créer une application de liste de tâches

- avoir une entête avec un titre, et un menu de deux éléments : "Ajouter une tâche", et "Liste des tâches". Chaque élément du menu est une ancre vers une section de la page.

- faire en sorte que, sur écran assez large, le titre et le menu soient affichés l'un à côté de l'autre. Et sur écran moins large, l'un en dessous de l'autre. Utiliser les media queries.

- avoir un pied de page avec une ou deux phrases de copyright ou autre.

- avoir un contenu principal avec les 2 sections les unes après les autres :
	- dans la section "Ajouter une tâche", avoir un formulaire avec un champ texte + un bouton pour soumettre le formulaire. Quand on soumet le formulaire, la tâche est ajoutée à la liste.
	- dans la section "Liste de tâches", avoir une liste de tâches où chaque tâche a :
		- une case à cocher permettant de définir une tâche comme faite
		- un bouton "supprimer" pour enlever la tâche de la liste
		- le texte décrivant la tâche

- la page prend 100% de l'espace visible : on veut que le pied de page soit ancré en bas de la page, et que, si jamais le contenu principal est trop grand pour la page, on scroll dans le contenu. Utiliser flexbox.


Côté JS, pour vous aider :

	étape 1 :

	- on a une <ul> vide, on la remplit au chargement de la page avec des tâches. Utiliser ces méthodes :
		- `document.querySelector()` et `document.querySelectorAll()` permettent de sélectionner un ou plusieurs éléments HTML dans votre JS, en passant un sélecteur CSS.
		- la méthode `forEach` sur un tableau ou une liste d'éléments retournés par querySelectorAll permet d'itérer sur le tableau. C'est comme un "for (...)"
		- `document.createElement()` permet de créer un nouvel élément HTML. Il n'est pas ajouté à la page tout seul : il est créé dans votre code JS. Vous pouvez ensuite manipuler cet élément (lui rajouter des classes, des attributs), et le rajouter où vous voulez précisément dans le HTML.
		- `unElement.classList` permet de lire et manipuler les classes d'un élément.
		- `unElement.setAttribute()` permet de définir un attrit sur un élément.
		- `unElement.textContent` permet de définir le contenu texte d'un élément.
		- `unElement.append()` permet d'ajouter un enfant à un élément. C'est via cette méthode qu'on peut ajouter un élément sur la page.

	étape 2 :

	- faire marcher le formulaire de création
		- `unElement.addEventListener()` permet de rajouter un gestionnaire d'événement sur un élément HTML. C'est via ceci qu'on va écouter quand on clique sur un bouton, quand on soumet un formulaire, quand on clique sur une case à cocher, etc.
		- `event.preventDefault()` permet d'intercepter le comportement par défaut d'un événement. Par exemple, quand on écoute l'événement de soumission d'un formulaire, on voudra utiliser ceci pour gérer en JS le comportement, et éviter que le HTML soumette le formulaire et recharge la page comme habituellement.
		- la propriété `value` d'un élément `input` contient le texte renseigné par l'utilisateur

	étape 3 :

	- pouvoir supprimer un élément
		- besoin de :
			- soit faire de la délégation d'évenements pour gérer le check/uncheck de la case à cocher
			- soit changer le code de création d'un item pour rajouter un événement
		- `unElement.remove()` permet de supprimer un élément de la page

	étape 4 :

	- rajouter une checkbox sur un élément de liste et rendre une tâche faite ou pas (la barrer)
		- besoin de changer le code de création d'un item pour rajouter une checkbox
